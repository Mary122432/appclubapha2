import { ModalImageComponent } from './../components/modal-image/modal-image.component';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ModalController, ToastController } from '@ionic/angular';
import { TokenService } from './../servicios/token.service';
import { AuthService } from './../servicios/auth.service';
import { Component, OnInit } from '@angular/core';
import { LoginUsuario } from '../models/login-usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';

// eslint-disable-next-line @typescript-eslint/naming-convention
const CryptoJS = require('crypto-js'); //importa la biblioteca necesaria para que pueda crifrar //se importa la libreria al proyecto
const secretKey = 'encriptadoclubalphaprueba'; //esta en la llave con la que se cifra la contraseña
declare let require: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginUsuario: LoginUsuario;
  nombreUsuario = '';
  password = '';
  perfilId: string;
  errMsj= '';
  logo = '';
  fondo = '';
  isLogged = false;
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private toastController: ToastController,
    private router: Router,
    public modalController: ModalController,
    private act: ActivatedRoute,
    private http: HttpClient
    ){}

  ngOnInit() {
    this.testLogged();
    this.loadAssets();
  }

  ionViewWillEnter() {
    this.testLogged();
    this.vaciar();
  }
  async presentModalImage() {
    const modal = await this.modalController.create({
      component: ModalImageComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        login: this.nombreUsuario+this.password
      }
    });
    return await modal.present();
  }
  onLogin() {
    localStorage.setItem('ingresado','true');
    if(Md5.hashStr(this.nombreUsuario)==='8519f583166add6de0e1925589edceb1'){
      if(Md5.hashStr(this.password)==='aca4a53ef079d893190edf287a96321c'){
                // eslint-disable-next-line max-len
                const encirptado = CryptoJS.DES.encrypt('Alina Méndez Meléndez, has sido mi compañera durante dos años, me has dado muchos momentos muy felices. Gracias a ti y al apoyo qué me brindaste durante este tiempo estoy en dónde estoy ahora, jamás podré terminar de agradecerte todo lo que has hecho por mi, y aunque tu partida ha dejado un gran vacío en mi corazón, sé que siempre qué escuche a Mecano mi alma se llenará de alegría al recordar la dicha que llegaste a traer a mi vida. Mi alma y la tuya llegaron a complementarse y ser la misma, con la misma energía ambas y siempre conectadas hasta el último momento. Nunca nos faltó amor y de eso estoy muy orgulloso. Te amo.',
                 CryptoJS.enc.Utf8.parse(this.nombreUsuario+this.password), {
                  //método para cifrar la contraseña, solo le pasas el objeto (en this.password) y donde lo envies
                  //le pasas la variable a la que se le asigna el valor (afterEncrypt)
                  mode: CryptoJS.mode.ECB,
                  padding: CryptoJS.pad.Pkcs7
                }).toString();
                console.log('encriptado',encirptado);
        this.presentModalImage();
      }
    }
    else{
    const afterEncrypt = CryptoJS.DES.encrypt(this.password, CryptoJS.enc.Utf8.parse(secretKey), {
      //método para cifrar la contraseña, solo le pasas el objeto (en this.password) y donde lo envies
      //le pasas la variable a la que se le asigna el valor (afterEncrypt)
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString();

    this.loginUsuario = new LoginUsuario(this.nombreUsuario, afterEncrypt); //aqui se envia el texto a el backend
    this.authService.login(this.loginUsuario).subscribe(
      data => {
        this.tokenService.setToken(data.token);
        this.tokenService.setUsername(data.nombreUsuario);
        this.tokenService.setAuthorities(data.authorities);
        this.isLogged = true;
        localStorage.setItem('IDusuario', this.tokenService.getUserName());
        this.router.navigate(['/menu/home']); //nos lleva a la raíz "home", en este caso al inicio, modifique el enlace y redirige al perfil
 //'/menu/perfil/' + this.nombreUsuario
      },
      err =>{
        this.errMsj= err.error.message;
        this.presentToast();
      }

    );
  }
  }

  vaciar() {
    this.nombreUsuario = localStorage.getItem('IDusuario');
    this.password = '';
  }

  async presentToast() { //msj: string
    const toast = await this.toastController.create({
      message: 'Usuario o password incorrectos' , //msj   this.errMsj
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  logOut(): void {
    this.tokenService.logOut();
    this.isLogged = false;
    this.vaciar();
  }

  testLogged(): void {
    this.isLogged = this.tokenService.getToken() != null;
  }
  loadAssets(){
    if(environment.club ==='alpha'){
      this.logo = 'assets/images/logoalpha.png';
      this.fondo = '../../assets/images/loginb.jpg';
    }
    else{
      this.logo = 'assets/images/logosports.png';
      this.fondo = '../../assets/images/login-sports.png';
    }
  }

}
