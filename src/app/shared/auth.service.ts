import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import firebase from 'firebase/app';
import User = firebase.User;
import auth = firebase.auth;
import UserCredential = firebase.auth.UserCredential;
import {DatePipe} from '@angular/common';
import {UserModel} from './user.model';


@Injectable({providedIn: 'root'})
export class AuthService {
    user: UserModel = new UserModel();
    isAuthenticated = false;

    constructor(private afAuth: AngularFireAuth,
                private afs: AngularFirestore,
                private datePipe: DatePipe) {

    }

    public updateUser(user: User, userName?: string): Promise<void> {
        return this.afs.doc(`users/${user.uid}`).set({
            uid: user.uid,
            displayName: userName || user.displayName,
            email: user.email,
            photoURL: user.photoURL,
            registerDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss'),
            lastDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
        });
    }

    signWithAnonymous() {
        return this.afAuth.signInAnonymously()
            .then(credential => {
                return this.updateUser(credential.user);
            });
    }

    signWithEmail(email: string, password: string): Promise<UserCredential> {
        return this.afAuth.signInWithEmailAndPassword(email, password);
    }

    signUp(email: string, password: string) {
        return this.afAuth.createUserWithEmailAndPassword(email, password);
    }

    signOut() {
        return this.afAuth.signOut();
    }


    githubLogin() {
        this.afAuth.signInWithPopup(new auth.GithubAuthProvider())
            .then(credential => {
                return this.updateUser(credential.user);
            });
    }

    googleLogin() {
        this.afAuth.signInWithPopup(new auth.GoogleAuthProvider())
            .then(credential => {
                return this.updateUser(credential.user);
            });
    }

    twitterLogin() {
        this.afAuth.signInWithPopup(new auth.TwitterAuthProvider())
            .then(credential => {
                return this.updateUser(credential.user);
            });
    }

    facebookLogin() {
        this.afAuth.signInWithPopup(new auth.FacebookAuthProvider())
            .then(credential => {
                return this.updateUser(credential.user);
            });
    }
}
