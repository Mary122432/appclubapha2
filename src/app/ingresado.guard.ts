import { NavController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from 'src/app/servicios/token.service';

@Injectable({
  providedIn: 'root'
})
export class IngresadoGuard implements CanActivate {

  constructor(public navCtrl: NavController, private TS:TokenService){}
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.TS.getUserName()){
        this.navCtrl.navigateRoot('menu/home');
        return false;
      }else{
        return true;
      }    
    /*if(localStorage.getItem('ingresado')){
      return true;
    }else{
      this.navCtrl.navigateRoot('login');
      return false;
    }*/
  }
  
}
