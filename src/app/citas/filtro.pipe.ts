import { Pipe, PipeTransform } from '@angular/core';
import { Citasmodels } from '../models/citasmodels';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(citas:Citasmodels[], texto: string): Citasmodels[] {


    if(texto.length === 0 ){return citas;}

    texto = texto.toLocaleLowerCase();

    return citas.filter( citas => {
        return citas.nombre.toLocaleLowerCase().includes(texto) || citas.hora.toLocaleLowerCase().includes(texto)
        || citas.lugar.toLocaleLowerCase().includes(texto);

    });

  }

}
