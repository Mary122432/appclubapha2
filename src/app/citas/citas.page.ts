import { ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { IonContent, IonRefresher, ToastController } from '@ionic/angular';
import {CitasService}  from './../servicios/citas.service';
import { ClienteService } from '../servicios/cliente.service';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Citasmodels,CitaApartada } from './../models/citasmodels';
import { TokenService } from '../servicios/token.service';
import { environment } from './../../environments/environment';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import 'moment/locale/es';
moment.locale('es');

@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {

  @ViewChild(IonContent, {static: true}) content: IonContent;

  semana: any=[
    'Lunes',
    'Martes',
    'Xiercoles',
    'Jueves',
    'Viernes',
    'Sabado',
    'Domingo'
  ];

  selectType = 'APARTAR CITAS';
  segmentList = ['APARTAR CITAS','VER MIS CITAS'];
  citas: Citasmodels[]=[];
  TextoBuscar='';
  citasusuario: CitaApartada[];
  monthSelect: any[];
  dateSelect: any;
  cliente: any;
  datos: any;
  fechaactual = moment().format('LL');
  hoy=  moment().format('YYYY-MM-DD');
  fechaSeleccionada = this.fechaactual;
  fechaSeleccionada2: any;
  seleccionfecha: any;
  seleccionfecha2: any;
  isLoading = true;
  nombreclub: string;
  fechaCita: any;
  citaSelect: any;
  idcita: any;
  respuesta: any;
  opts= {
    freeMode:true,
    slidesPerView:2.1,
    slidesOffsetBefore:40,
    slidesOffsetAfter:100
  };
  colores: Citasmodels[]=[];
  diia:any;
  valueSelected:string='todos';

  constructor(private citasService: CitasService, private httpClient: HttpClient,
    private TS: TokenService, private CS: CitasService, private alertController: AlertController,
    private ar: ActivatedRoute, private clienteService: ClienteService, private toastController: ToastController,
    private changeRef: ChangeDetectorRef) { }

  ngOnInit(): void{
    const mes = moment().format('MM');
    const year = moment().format('YYYY');
    this.getDaysFromDate(mes,year);
    this.MuestraCitaDiaActual();
    this.seleccionfecha =  moment().format('YYYY-MM-DD');
    this.muestraCitas();
    this.coloresfiltro();
    this.MuestraColorDiaActual();
  }

  ionViewWillEnter(){
    this.muestraCitas();
  }

//Muestra el calendario
  getDaysFromDate(month,year){
    const startDate = moment(`${year}/${month}/01`);
    const endDate = startDate.clone().endOf('month');
    this.dateSelect=startDate;
    const diffDays = endDate.diff(startDate, 'days', true);
    const numberDays = Math.round(diffDays);
    const arrayDays = Object.keys([ ...Array(numberDays)]).map((a: any) =>{
      a= parseInt(a, 10) + 1;
      const dayObject = moment(`${year}-${month}-${a}`);
      return{
        name: dayObject.format('LLLL'),
        value:a,
        indexWeek: dayObject.isoWeekday()
      };
    });
    this.monthSelect = arrayDays;
  }

//Metodo para cambiar la vista del mes
  changeMonth(flag){
    if(flag < 0){
      const prevDate = this.dateSelect.clone().subtract(1, 'month');
      this.getDaysFromDate(prevDate.format('MM'), prevDate.format('YYYY'));
    }else{
      const nextDate = this.dateSelect.clone().add(1, 'month');
      this.getDaysFromDate(nextDate.format('MM'), nextDate.format('YYYY'));
    }
  }

  //Metodo donde se muestran las citas del dia actual
  MuestraCitaDiaActual(){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe((data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;
      //localStorage.getItem(dato);
      const monthYear = moment().format('YYYY-MM');
      const day = moment().format('D'); //'DD'
      const parse = `${monthYear}-${day}`
      console.log(parse);
      const params = new HttpParams()
        .set('usuario', this.TS.getUserName())
        .set('club', dato)
        .set('dia', parse );
      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((dataC: any) =>{
        console.log(dataC);
        this.citas= dataC;
      },err=>{
        console.log(err);
      });
    },);
  }

  //Metodo donde se muestran las citas del dia seleccionado
  clickDay(day){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe(
      (data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;

      const monthYear = this.dateSelect.format('YYYY-MM');
      //console.log('day',day.value.toString().padStart(2, '0'));
      const parse = `${monthYear}-${day.value.toString().padStart(2, '0') }`;
      this.seleccionfecha=parse;

      const mes = this.dateSelect.format('MMMM');
      const year = this.dateSelect.format('YYYY');
      const fechacompleta = `${day.value} de ${mes} de ${year}`;
      this.diia=day.value;
      this.fechaSeleccionada= fechacompleta;


      console.log(parse);

      const params = new HttpParams()
      .set('usuario', this.TS.getUserName())
      .set('club', dato)
      .set('dia', parse );

      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;

      // eslint-disable-next-line @typescript-eslint/no-shadow
      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((data: any) =>{
        console.log(data)
        this.citas= data;

        const C= this.idcita= data.id;
        console.log(C);
    },
      err=>{
      console.log(err);
      }
    );
    },
    );
  }

  goTop() {
    this.content.scrollToTop(2000);
  }

  info(){
    this.citasService.obtClases().subscribe((data: any)=>{
    this.datos = data;
  });
}

  Reserva(idCita: string){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe((data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;
      const datos = { usuario: this.TS.getUserName(), dia: this.seleccionfecha, club: dato, id: idCita};
      //console.log(datos);
      const options = {
        headers: {
          'Content-Type': 'application/json',
          //'responseType': 'text/plain;charset=UTF-8'
        }
      };
      return new Promise((resolve, reject) => this.httpClient.post(environment.global+'citas/crearApartados',
      JSON.stringify(datos), options).toPromise().then(async (res: any) =>{
        resolve(res);
        console.log(res);
        const d = this.respuesta = res.Respuesta;
        this.presentToast(d);
        //this.doRefresh(this.refresher.ionPull);
        //console.log(d);
      },async (err)=>{
        reject(err);
        console.log(err);
        const d= this.respuesta = err.error.Respuesta;
        this.presentToast(d);
        //console.log(d);
      }));
    });
  }

  segmentChanged(event){
  //console.log(event.target.value);
  //this.pList = this.allList.sort(()=> Math.random() - 0.5);
  }

  segmentChanged2(event:CustomEvent){
    this.valueSelected =event.detail.value;

    console.log(this.valueSelected);
    //this.pList = this.allList.sort(()=> Math.random() - 0.5);
    }

  async presentToast(msj: string) { //msj: string
    const toast = await this.toastController.create({
      message: msj , //msj   this.errMsj
      duration: 2000,
      position: 'bottom' //top, middle
    });
    toast.present();
  }

  borrarCita(citaCancelada: CitaApartada){
    console.log('cita borrada');
    const datos = { usuario: this.TS.getUserName(), dia: citaCancelada.dia, id:citaCancelada.id};
    //console.log(datos);   id: idcita
    const options = {
      headers: {
        'Content-Type': 'application/json',
        //'responseType': 'text/plain;charset=UTF-8'
      }
    };
    return new Promise((resolve, reject) => this.httpClient.post(environment.global+'citas/cancelarApartado',
    JSON.stringify(datos),options).toPromise().then(async (res: any) =>{
      resolve(res);
      console.log(res);
      const d= this.respuesta = res.Respuesta;
      this.presentToast(d);
      console.log(d);
    },async (err)=>{
      reject(err);
      console.log(err);
      const d= this.respuesta = err.error.Respuesta;
      this.presentToast(d);
      console.log(d);
    }));
  }

  async EliminarCita(citaCancelada: CitaApartada){
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: '¿Seguro que desea eliminar esta cita?',
      buttons: [{
        text: 'Aceptar',
        handler:()=>{
          this.borrarCita(citaCancelada);
          this.muestraCitas();
          this.changeRef.detectChanges();
          //this.doRefresh(this.refresher.ionPull);
        }
      },{
          text: 'Cancelar',
          role:'cancel',
          cssClass: 'secondary',
        }]
    });
    await alert.present();
  }

  cargar(){
    this.CS.obtClases().subscribe((data: any)=>{
      console.log(data);
      this.citas=data;
      //var C= this.citas= data.id;
      //console.log(C);
    },err =>{
     console.log(err);
    });
  }

  muestraCitas(){ //day
    //const monthYear = this.dateSelect.format('YYYY-MM')
    //const parse = `${monthYear}-${day.value}`
    //console.log(parse);
    this.CS.MuestraCitas().subscribe((data: any)=>{
    this.citasusuario=data;
      console.log('Mostrando citas');
      //var F= this.fechaCita= JSON.parse(JSON.stringify(data.dia));
      console.log(data);
      //console.log(F);
    });
  }

  actionMethod($event: MouseEvent) {
    ($event.target as HTMLButtonElement).disabled = true;
  }

  doRefresh(event) {
    this.CS.MuestraCitas().subscribe(data=>{
      this.citasusuario=data;
      console.log(data);
    }
    );
    setTimeout(() => {
      //console.log('Async operation has ended');
      event.target.complete();
    });
  }

  buscarCategoria(event){
    const texto = event.target.value;
    this.TextoBuscar =texto;
  }


  coloresfiltro(){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe((data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;
      //localStorage.getItem(dato);
      const monthYear = moment().format('YYYY-MM');
      const day = moment().format('D'); //'DD'
      const parse = `${monthYear}-${day}`
      console.log(parse);
      const params = new HttpParams()
        .set('usuario', this.TS.getUserName())
        .set('club', dato)
        .set('dia', parse );
      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((dataC: any) =>{
        //console.log(dataC);
        //let hash = {};
        //dataC = dataC.filter(o => hash[o.color] ? false : hash[o.color] = true);
        //console.log(dataC);
        //this.colores= dataC;

        let hash = {};
        dataC = dataC.filter(o => hash[o.color] ? true : hash[o.color] = true);
        console.log("Nombres iguales:");
        console.log(dataC);
        this.colores= dataC;

      },err=>{
        console.log(err);
      });
    },);
  }

  colorpordia(day){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe(
      (data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;

      const monthYear = this.dateSelect.format('YYYY-MM');
      const parse = `${monthYear}-${day.value.toString().padStart(2, '0')}`;
      this.seleccionfecha=parse;

      const mes = this.dateSelect.format('MMMM');
      const year = this.dateSelect.format('YYYY');
      const fechacompleta = `${day.value} de ${mes} de ${year}`;
      this.fechaSeleccionada= fechacompleta;

      console.log(parse);

      const params = new HttpParams()
      .set('usuario', this.TS.getUserName())
      .set('club', dato)
      .set('dia', parse );
      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
      // eslint-disable-next-line @typescript-eslint/no-shadow
      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((data: any) =>{
        let hash = {};
        data = data.filter(o => hash[o.color] ? false : hash[o.color] = true);
        console.log(data);
        this.colores= data;
    },
      err=>{
      console.log(err);
      }
    );
    },
    );
  }

  MuestraColorDiaActual(){
    const id = this.ar.snapshot.params.id;
    this.clienteService.obtCliente(id).subscribe((data: any)=>{
      this.cliente=data;
      const dato= this.nombreclub = data.club.nombre;
      //localStorage.getItem(dato);
      const monthYear = moment().format('YYYY-MM');
      const day = moment().format('D'); //'DD'
      const parse = `${monthYear}-${day}`
      console.log(parse);
      const params = new HttpParams()
        .set('usuario', this.TS.getUserName())
        .set('club', dato)
        .set('dia', parse );
      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((data: any) =>{
        let hash = {};
        data = data.filter(o => hash[o.color] ? false : hash[o.color] = true);
        console.log(data);
        this.colores= data;
      },err=>{
        console.log(err);
      });
    },);
  }



}
