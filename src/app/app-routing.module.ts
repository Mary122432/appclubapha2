import { NoIngresadoGuard } from './no-ingresado.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IngresadoGuard } from './ingresado.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
     ,canActivate:[IngresadoGuard]
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
    //, canActivate:[NoIngresadoGuard]
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'noticias',
    loadChildren: () => import('./pages/noticias/noticias.module').then( m => m.NoticiasPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'adquiere',
    loadChildren: () => import('./pages/adquiere/adquiere.module').then( m => m.AdquierePageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'compras',
    loadChildren: () => import('./pages/compras/compras.module').then( m => m.ComprasPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'cardio-dance',
    loadChildren: () => import('./pages/entrenamientos/cardio-dance/cardio-dance.module').then( m => m.CardioDancePageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
    ,canActivate:[IngresadoGuard]
  },
  {
    path: 'movimientos/:id',
    loadChildren: () => import('./movimientos/movimientos.module').then( m => m.MovimientosPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'citas/:id', //   /:id
    loadChildren: () => import('./citas/citas.module').then( m => m.CitasPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'misdatos/:id',
    loadChildren: () => import('./misdatos/misdatos.module').then( m => m.MisdatosPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'credencial',
    loadChildren: () => import('./pages/credencial/credencial.module').then( m => m.CredencialPageModule)
    ,canActivate:[NoIngresadoGuard]
  },
  {
    path: 'lista-retos',
    loadChildren: () => import('./pages/lista-retos/lista-retos.module').then( m => m.ListaRetosPageModule)
    ,canActivate:[NoIngresadoGuard]
  },  {
    path: 'pass',
    loadChildren: () => import('./pages/pass/pass.module').then( m => m.PassPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
