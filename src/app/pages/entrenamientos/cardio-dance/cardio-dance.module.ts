import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardioDancePageRoutingModule } from './cardio-dance-routing.module';

import { CardioDancePage } from './cardio-dance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardioDancePageRoutingModule,
    ComponentsModule
  ],
  declarations: [CardioDancePage]
})
export class CardioDancePageModule {}
