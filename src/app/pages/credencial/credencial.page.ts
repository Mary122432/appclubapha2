import { AlertController } from '@ionic/angular';
import { ClienteService } from './../../servicios/cliente.service';
import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { timer } from 'rxjs';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-credencial',
  templateUrl: './credencial.page.html',
  styleUrls: ['./credencial.page.scss'],
})
export class CredencialPage implements OnInit {

  public cards: any;
  public cliente: Cliente;
  isLoading = true;
  //fondo = '../../assets/images/cardactivasp.png';
  constructor(private clienteService: ClienteService,
    private alertController: AlertController) {


  }
  ngOnInit() {
     this.clienteService.cargarDatosCliente().then(miCliente=>{this.cliente=miCliente;
      console.log('Mi cliente ',miCliente);
      this.cards = [
        {
          state: 'Activa',
          logo: 'assets/images/splogorectangulo.png',
          a: 'Membresia',
          b: 'Basica',
          c: 8283,
          d: 2264,
          expires: '24/09',
          bank: this.cliente.club.nombre
        }
      ];
      this.isLoading = false;
    });
  }

  getQrCode(): string{
    return 'CLIENTE-'+this.cliente.idCliente+'-'+Md5.hashStr(this.cliente.noMembresia.toString());
  }

}
