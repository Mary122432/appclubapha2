import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../servicios/token.service';
import { environment } from '../../../environments/environment';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-pass',
  templateUrl: './pass.page.html',
  styleUrls: ['./pass.page.scss'],
})
export class PassPage implements OnInit {

  password = "";
  passwordact = "";
  respuesta:any;

  constructor(private TS:TokenService, private httpClient:HttpClient, private toastController:ToastController) { }

  ngOnInit() {
  }

  CambiarContra(){
    const datos = { usuario: this.TS.getUserName(), passwordAnterior: this.password, passwordNuevo: this.passwordact};
    //console.log(datos); 
    const options = {
      headers: {
        'Content-Type': 'application/json',
        //'responseType': 'text/plain;charset=UTF-8'
      }
    };
    return new Promise((resolve, reject) => this.httpClient.post(environment.global+'auth/new-password',
    JSON.stringify(datos), options).toPromise().then(async (res: any) =>{
      resolve(res);
      console.log(res);
      const d = this.respuesta = "Contraseña cambiada con exito";
      this.presentToast(d);
      console.log(d);
    },async (err)=>{
      reject(err);
        console.log(err);
        const d= this.respuesta = err.error.respuesta;
        this.presentToast(d + " ,vuelve a intentarlo");
        //console.log(d);
    }));
  }

  async presentToast(msj: string) { //msj: string
    const toast = await this.toastController.create({
      message: msj , //msj   this.errMsj
      duration: 2000,
      position: 'middle' //top, middle
    });
    toast.present();
  }

}
