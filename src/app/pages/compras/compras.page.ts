import { TokenService } from './../../servicios/token.service';
import { Pase } from './../../models/pases';
import { PasesService } from '../../servicios/pases.service';
import { Component, OnInit } from '@angular/core';
import {timer} from 'rxjs';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {
  list: Array<Pase>;
  isLoading = true;
  // isLoading2 = true;

  constructor(private service: PasesService, private tokenService: TokenService) {
   // console.log('Mi userName es ',this.tokenService.getUserName());
  }

  ngOnInit() {
    this.service.getPases(Number(this.tokenService.getUserName())).subscribe(data=>{
      this.list=data;
      const newLocal = this;
      newLocal.isLoading = !this.isLoading;
      });

    // timer(2000).subscribe(r => {
    // });
  //   timer(2000).subscribe(r =>{
  //     this.isLoading2 = false;
  // });
  }
}
