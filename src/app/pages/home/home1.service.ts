import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class Home1Service {

    getBannerList(): Array<string> {
        return [
            'assets/images/list/1.jpg',
            'assets/images/list/2.jpg',
            'assets/images/list/3.jpg'
        ];
    }
    getBannersSports(){
      return[
        'assets/images/list/sportsplaza/banner1.jpg',
        'assets/images/list/sportsplaza/banner2.jpg',
        'assets/images/list/sportsplaza/banner3.jpg',
        'assets/images/list/sportsplaza/banner4.jpg',
        'assets/images/list/sportsplaza/banner5.jpg',
        'assets/images/list/sportsplaza/banner6.jpg'
      ];
    }
    getList(): Array<any> {
        return [
            {icon: 'happy-outline', title: 'Califica nuestro servicio',
            content: 'Podrás calificar nuestro servicio y para ayudarnos a mejorar.'},
            {icon: 'speedometer-outline', title: 'Niveles de entrenamiento',
             content: 'Nuestros asesores están preparados para asignarte una rutina acorde'},
            {
                icon: 'calendar-outline',
                title: 'Aparta tu clase o entrenamiento',
                content: 'Podrás apartar tu lugar en las distintas clases y salones.'
            },
            {icon: 'people-outline', title: 'Conoce a la comunidad', content: 'Interactua con otros usuarios a travez de retos.'}
        ];
    }
    getListSports(): Array<any> {
      return [
          {icon: 'scale-outline', title: 'FITNESS',
          content: '',
        modal: 'Cardio dance<br>Spinning<br>Fitness Attack<br>Step<br>Pilates<br>Extreme Workout<br>Ritmos latinos<br>GAP<br>Fitness Pump<br>Yoga<br>Zumba'},
          {icon: 'football-outline', title: 'ESCUELAS DEPORTIVAS',
           content: '',
        modal:' Box<br>Taekwondo<br>Baloncesto<br>Karate<br>Futbol'},
          {
              icon: 'calendar-outline',
              title: 'RENTA DE CANCHAS',
              content: '',
              modal:'Tenis<br>Duela de baloncesto<br>Futbol rápido<br>Futbol 7<br>Voleibol de playa<br>Squash.'
          },
          {icon: 'barbell-outline', title: 'GIMNASIO', content: '',
        modal:'Reserva tu visita una vez al día en intervalos de 1:20 horas, todos los días. Aplica restricciones.'}
      ];
  }
    getFList(): Array<any> {
        return [
            {
                title: 'Body Combat',
                content: 'Inspirado en las artes marciales donde se realizan golpes, puñetazos, patadas con coreografías musicales.'
            },
            {
                title: 'Indoor ciclyng',
                content: 'Es un ejercicio que consiste en rodar una bicicleta fija, donde se trabaja principalmente el tren inferior.'
            },
            {
                title: 'Body fitness',
                content: 'Es un entrenamiento donde se realiza un determinado número de circuitos con diferentes coreografías.'
            },
            {
                title: 'Fitness attack',
                content: 'Es un entrenamiento cardiovascular donde los participantes realizan puñetazos, patadas y golpes de combate.'
            },
        ];
    }

    getTList(): Array<any> {
        return [
            {name: 'Mónica Torroella', img: 'assets/images/avatar/2.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'Mario Rivera', img: 'assets/images/avatar/3.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'María José Murieda', img: 'assets/images/avatar/4.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'Elizabeth Ríos', img: 'assets/images/avatar/5.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'}
        ];
    }

    getNewList(): Array<any> {
        return [
            {title: 'Nuevo video de Spinning', img: 'assets/images/list/21.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {title: 'Reto "Piernas de acero"', img: 'assets/images/list/22.jpg',content: 'El nuevo reto "Piernas de acero" está disponible ahora.'},
            {title: 'Nuevos invitados', img: 'assets/images/list/23.jpg',content: 'Ahora hay nuevos invitados para el mes de agosto.'},
            {title: 'Artes marciales y el ejercicio', img: 'assets/images/list/24.jpg',content: 'Descubre las rutinas de ejercicio basadas en artes marciales'},
        ];
    }

}
