import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Home1Service } from './home1.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  list: Array<string>;
  list1: Array<any>;
  // fList: Array<any>;
  // tList: Array<any>;
  // newList: Array<any>;
  isLoading = true;

  option = {
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    autoplay: {
      delay: 2000,
      disableOnInteraction: false,
    }
  };
  constructor(private service: Home1Service, private alertController: AlertController) {
    this.loadAssets();
    //   this.fList = this.service.getFList();
    //   this.tList = this.service.getTList();
    //   this.newList = this.service.getNewList();
  }

  ngOnInit() {
    timer(500).subscribe(r => {
      this.isLoading = false;
    });
  }
  loadAssets() {
    if (environment.club === 'alpha') {
      this.list = this.service.getBannerList();
      this.list1 = this.service.getList();

    }
    else if (environment.club === 'sports') {
      this.list = this.service.getBannersSports();
      this.list1 = this.service.getListSports();

    }
  }
  async presentAlert(item: any) {
    //console.log('Presentando alert');
    const alert = await this.alertController.create({
      header: item.title,
      message: item.modal,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });
    await alert.present();
  }
}
