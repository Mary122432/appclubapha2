import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaRetosPage } from './lista-retos.page';

const routes: Routes = [
  {
    path: '',
    component: ListaRetosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaRetosPageRoutingModule {}
