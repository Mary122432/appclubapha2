import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaRetosPageRoutingModule } from './lista-retos-routing.module';

import { ListaRetosPage } from './lista-retos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaRetosPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListaRetosPage]
})
export class ListaRetosPageModule {}
