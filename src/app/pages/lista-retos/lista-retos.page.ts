import { SettingsService } from './../../servicios/settings.service';
import {Component, OnInit} from '@angular/core';
import {timer} from 'rxjs';
import { CardModel } from 'src/app/components/custom-card1/card.model';
import { Card1Service } from './card1.service';

@Component({
  selector: 'app-lista-retos',
  templateUrl: './lista-retos.page.html',
  styleUrls: ['./lista-retos.page.scss'],
})
export class ListaRetosPage implements OnInit {
    isLoading = true;
    isLoading2 = true;
    list: Array<CardModel>;

    constructor( private service: Card1Service, private settingsService: SettingsService) {
        this.list = this.service.getList();
    }


    ngOnInit() {
        timer(300).subscribe(r => {
            this.isLoading = !this.isLoading;
        });

        timer(2000).subscribe(r =>{
            this.isLoading2 = false;
        });
    }
    cambiarAlpha(){
      this.settingsService.enableAlpha();
    }
    cambiarSports(){
      this.settingsService.enableSportsPlaza();
    }
}
