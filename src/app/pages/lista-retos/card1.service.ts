/* eslint-disable max-len */
import {Injectable} from '@angular/core';
import { CardModel } from 'src/app/components/custom-card1/card.model';


@Injectable({providedIn: 'root'})
export class Card1Service {
    getList(): Array<CardModel> {
        return [
            {
                active: false,
                titulo: 'El más caminador',
                img: 'assets/images/list/61.jpg',
                inscritos: 23,
                fin: new Date('2021-11-16T00:00:00' ),
                icono: 'footsteps',
                tipo: 'Pasos caminados',
                descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                active: false,
                titulo: 'El más activo',
                img: 'assets/images/list/62.jpg',
                inscritos: 56,
                fin: new Date('2021-11-16T00:00:00' ),
                icono: 'fitness',
                tipo: 'Actividad',
                descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                active: false,
                titulo: 'Mayor recorrido',
                img: 'assets/images/list/61.jpg',
                inscritos: 78,
                fin: new Date('2021-11-16T00:00:00' ),
                icono: 'finger-print',
                tipo: 'Más visitas',
                descripcion: 'Cras iaculis pulvinar arcu non vehicula, consectetur adipiscing elit.'
            },
            {
                active: false,
                titulo: 'Reto nuevo',
                img: 'assets/images/list/62.jpg',
                inscritos: 90,
                fin: new Date('2021-11-16T00:00:00' ),
                icono: 'nutrition',
                tipo: 'Mejor dieta',
                descripcion: 'Thrilling visuals and the substantial chemistry of its well-matched leads make The Aeronauts an adventure well worth taking.'
            },
        ];
    }
}
