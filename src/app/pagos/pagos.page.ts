import { RespuestaOxxo } from './../models/pago-oxxo.model';
import { OxxoPayService } from './../servicios/oxxo-pay.service';
import { ModalController } from '@ionic/angular';
import { OxxoPayComponent } from './../components/oxxo-pay/oxxo-pay.component';
import { TokenService } from './../servicios/token.service';
import { PedidoService } from './../servicios/pedido.service';
import { Component, OnInit } from '@angular/core';
import { Pedido } from '../models/pedido';
import { timer } from 'rxjs';


@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.page.html',
  styleUrls: ['./pagos.page.scss'],
})
export class PagosPage implements OnInit {

  pedido: Pedido;
  total: number;
  isLoading = true;

  constructor(private tokenService: TokenService,
              private service: PedidoService,
              private modalController: ModalController,
              private oxxoPay: OxxoPayService) {
                this.service.getPedido(Number(this.tokenService.getUserName())).subscribe(data=>{
                  console.log('Mi pedido ',data);
                  this.pedido=data;
                  if(this.pedido)
                  {this.total = this.pedido.pedidoDetalle.reduce((total,detalle)=>total+detalle.importe,0);}
                });
              }

  ngOnInit() {
    timer(2000).subscribe(r =>{
      this.isLoading = false;
  });
  }
  async pagarOxxo(){
    const modal = await this.modalController.create({
      component: OxxoPayComponent,
      componentProps: {
        referencia: await this.getReferencia()
      }
    });
    return await modal.present();
  }
  async getReferencia(): Promise<RespuestaOxxo>{

    return await this.oxxoPay.obtenerReferencia(Number(this.total.toFixed(2))).toPromise();
  }
}
