import { ComprasPageModule } from './../pages/compras/compras.module';
import { HomePageModule } from './../pages/home/home.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngresadoGuard } from '../ingresado.guard';
import { NoIngresadoGuard } from '../no-ingresado.guard'

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children:[
      {
        path: 'comprade-servicios',
        loadChildren: () => import('../comprade-servicios/comprade-servicios.module').then( m => m.CompradeServiciosPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'perfil/:id',
        loadChildren: () => import('../perfil/perfil.module').then( m => m.PerfilPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'movimientos/:id',
        loadChildren: () => import('../movimientos/movimientos.module').then( m => m.MovimientosPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'pagos',
        loadChildren: () => import('../pagos/pagos.module').then( m => m.PagosPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'anuncios',
        loadChildren: () => import('../anuncios/anuncios.module').then( m => m.AnunciosPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'formp',
        loadChildren: () => import('../formp/formp.module').then( m => m.FormpPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then( m => m.HomePageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'noticias',
        loadChildren: () => import('../pages/noticias/noticias.module').then( m => m.NoticiasPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'adquiere',
        loadChildren: () => import('../pages/adquiere/adquiere.module').then( m => m.AdquierePageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'compras',
        loadChildren: () => import('../pages/compras/compras.module').then( m => m.ComprasPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'cardio-dance',
        loadChildren: () => import('../pages/entrenamientos/cardio-dance/cardio-dance.module').then( m => m.CardioDancePageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'citas/:id', // /:id
        loadChildren: () => import('../citas/citas.module').then( m => m.CitasPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
        path: 'credencial',
        loadChildren: () => import('../pages/credencial/credencial.module').then( m => m.CredencialPageModule)
        ,canActivate:[NoIngresadoGuard]
      },
      {
      path: 'retos',
      loadChildren: () => import('../pages/lista-retos/lista-retos.module').then( m => m.ListaRetosPageModule)
      ,canActivate:[NoIngresadoGuard]
      },
      {
        path:'login',
        loadChildren: () => import('../login/login.module').then( m => m.LoginPageModule)
        ,canActivate:[IngresadoGuard]
      },
      {
        path:'pass',
        loadChildren: () => import('../pages/pass/pass.module').then( m => m.PassPageModule)
        ,canActivate:[NoIngresadoGuard]
      }


    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
