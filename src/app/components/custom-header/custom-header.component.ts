import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {MenuController} from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'custom-header',
    templateUrl: './custom-header.component.html',
    styleUrls: ['./custom-header.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CustomHeaderComponent {
    isActive = false;
    @Input() title = 'Home';
    @Input() hidden = true;
    @Output() onSearch = new EventEmitter();
    @Input() hiddenAbout = true;
    sportsPlaza = false;
    constructor(private menu: MenuController) {
    }

    toggle() {
        this.isActive = !this.isActive;
    }

    search(event) {
        this.onSearch.emit(event);
    }

    onOpen() {
        this.menu.enable(true, 'custom');
        this.menu.open('custom');
    }
    loadAssets(){
      if(environment.club ==='alpha'){
      }
      else if(environment.club ==='sports'){
        this.sportsPlaza = true;
      }
    }
}
