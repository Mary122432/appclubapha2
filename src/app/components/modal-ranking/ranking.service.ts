import { RankingModel } from './../../models/ranking.model';
/* eslint-disable max-len */
import {Injectable} from '@angular/core';


@Injectable({providedIn: 'root'})
export class RankingService {
    getList(): Array<RankingModel> {
        return [
            {
              posicion: 1,
              avatar: 'assets/images/avatar/7.jpg',
              nombre: 'María Elena',
              total: 15642,
            },
            {
              posicion: 2,
              avatar: 'assets/images/avatar/8.jpg',
              nombre: 'Fernanda',
              total: 14893,
            },
            {
              posicion: 3,
              avatar: 'assets/images/avatar/10.jpg',
              nombre: 'Vanessa',
              total: 14750,
            },
            {
              posicion: 4,
              avatar: 'assets/images/avatar/11.jpg',
              nombre: 'Erika',
              total: 13468,
            },
            {
              posicion: 5,
              avatar: 'assets/images/avatar/12.jpg',
              nombre: 'Alina',
              total: 13024,
            },
            {
              posicion: 6,
              avatar: 'assets/images/avatar/13.jpg',
              nombre: 'Gabriela',
              total: 10547,
            },
            {
              posicion: 7,
              avatar: 'assets/images/avatar/14.jpg',
              nombre: 'Sofía',
              total: 10236,
            },
            {
              posicion: 8,
              avatar: 'assets/images/avatar/15.jpg',
              nombre: 'Teresa',
              total: 9058,
            },
            {
              posicion: 9,
              avatar: 'assets/images/avatar/16.jpg',
              nombre: 'Carolina',
              total: 8956,
            },
            {
              posicion: 10,
              avatar: 'assets/images/avatar/17.jpg',
              nombre: 'Sarahi',
              total: 8254,
            }
        ];
    }
}
