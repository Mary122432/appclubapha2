import { ModalController } from '@ionic/angular';
import { RankingService } from './ranking.service';
import { Component, OnInit } from '@angular/core';
import { RankingModel } from 'src/app/models/ranking.model';

@Component({
  selector: 'app-modal-ranking',
  templateUrl: './modal-ranking.component.html',
  styleUrls: ['./modal-ranking.component.scss'],
})
export class ModalRankingComponent implements OnInit {
  ranking: RankingModel[];
  constructor(private service: RankingService,
    private modalController: ModalController) {
    this.ranking=service.getList();
  }

  ngOnInit() {}
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss();
  }
}
