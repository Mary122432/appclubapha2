import { ModalRankingComponent } from './../modal-ranking/modal-ranking.component';
import {Component, Input} from '@angular/core';
import { ModalController } from '@ionic/angular';
import {CardModel} from './card.model';

@Component({
    selector: 'custom-card1',
    templateUrl: './custom-card1.component.html',
    styleUrls: ['./custom-card1.component.scss'],
})
export class CustomCard1Component {
    @Input() list: Array<CardModel>;

    constructor(public modalController: ModalController) {
    }
    segmentChanged(ev: any) {
      console.log('Segment changed', ev.detail.value);
    }
    async cargarRanking(){
      const modal = await this.modalController.create({
        component: ModalRankingComponent,
      });
      return await modal.present();
    }
}
