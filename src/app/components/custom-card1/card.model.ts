export class CardModel {
  public active: boolean;
  public img: string;
  public titulo: string;
  public fin: Date;
  public tipo: string;
  public icono: string;
  public inscritos: number;
  public descripcion: string;
}
