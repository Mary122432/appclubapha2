import { PipesModule } from './pipes/pipes.module';
import { interceptorProvider } from './interceptors/foto-interceptor.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import {LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {ComponentsModule} from './components/components.module';
import { IonicStorageModule } from '@ionic/storage';
import { SpinnerInterceptor } from './interceptors/spinner.interceptor';
import localeEsMx from '@angular/common/locales/es-MX';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEsMx,'es-MX');
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],

  imports: [BrowserModule, ComponentsModule,
    IonicModule.forRoot(), AppRoutingModule,PipesModule,NgxQRCodeModule,
    HttpClientModule,ComponentsModule,BrowserAnimationsModule,IonicStorageModule.forRoot(),
    ],

  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    { provide: LOCALE_ID, useValue: 'es-MX'},
    { provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi:true},
  SplashScreen,StatusBar,ScreenOrientation,interceptorProvider],
  bootstrap: [AppComponent],
})
export class AppModule {}
