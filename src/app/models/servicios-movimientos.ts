export class ServiciosMovimientos {
    id?: number; //opcional
    idClienteMovimiento: number;
    idCliente: number;
    cliente: string;
    concepto: string;
    fechaDeAplicacion: Date;
    idOrdenDeVenta: number;
    idOrdenDeVentaDetalle: number;
    credito: number;
    debito: number;
    saldo: number;
    activo: boolean;
    fechaCreacion: Date;
    fechaModificacion: Date;
}
