// Generated by https://quicktype.io

export interface RespuestaOxxo {
  numeroReferencia: string;
  fechaExpiracion:  string;
  totalPago:        number;
}
