export class RankingModel {
  posicion: number;
  avatar: string;
  nombre: string;
  total: number;
}
