import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {finalize} from 'rxjs/operators';
import { SpinnerService } from '../servicios/spinner.service';

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor{
    constructor(private spnr:SpinnerService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spnr.show();
        return next.handle(req).pipe(
            finalize(()=> this.spnr.hide()));
       
    }

}