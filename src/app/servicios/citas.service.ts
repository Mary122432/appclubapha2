import { Citasmodels, CitaApartada } from './../models/citasmodels';
import { environment } from './../../environments/environment';
import { TokenService } from './token.service';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { Cliente } from './../models/cliente';
import { Observable } from 'rxjs';
//import { RequestOptions } from 'http';


@Injectable({
  providedIn: 'root'
})
export class CitasService {

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

      //Obtiene una lista de las clases del dia
      public obtClases(): Observable<Citasmodels[]>{

        const params = new HttpParams()
	      .set('usuario', this.tokenService.getUserName())
	      .set('club', 'Club Alpha 2') //obtener club de los datos del cliente
        .set('dia', '2021-10-21');

        const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
        console.log( fullURL );
        return this.httpClient.get<Citasmodels[]>(fullURL);
      }

      //Metodo post que envia los datos del usuario para reservar cita
      ReservaCita() {
        const datos = { usuario: this.tokenService.getUserName(), dia: '2021-10-29', club: 'Club Alpha 2', id:'dd99dffe-49d0-4f27-b582-7eb6bc8feda9'};

        const options = {
           headers: {
            'Content-Type': 'application/json',
            //'responseType': 'text/plain;charset=UTF-8'
          }
        };

        return this.httpClient.post(environment.global+'citas/crearApartados', JSON.stringify(datos), options).toPromise();  //.toPromise()
      }

      MuestraCitas():Observable<CitaApartada[]>{

          const params = new HttpParams()
          .set('usuario', this.tokenService.getUserName());

          const fullURL = `${environment.global+'citas/obtenerApartadosUser'}?${params.toString()}`;
          console.log( fullURL );
          return this.httpClient.get<CitaApartada[]>(fullURL);


      }

}
