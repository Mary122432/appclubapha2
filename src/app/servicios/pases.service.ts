import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pase } from '../models/pases';

@Injectable({
  providedIn: 'root'
})
export class PasesService {

  constructor(private http: HttpClient) { }

   public getPases(id: number): Observable<Pase[]>{
    return this.http.get<Pase[]>(environment.global + `citas/obtenerPase/${id}`);
  }
}
