import { environment } from './../../environments/environment';
import { TokenService } from './token.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from './../models/cliente';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  idusuario:any;
  constructor(private httpClient: HttpClient, private tokenService: TokenService, private storage: Storage) { }

    //Este metodo sirve para mostrar un cliente, no necesita concatenar el id del cliente,
    //aunque no lo pida  otra opcion :`obtenerCliente/${id}`
    public obtCliente(id: number): Observable<Cliente[]>{

      const cliente = this.httpClient.get<Cliente[]>(environment.global +'alpha/obtenerCliente/' +
      this.tokenService.getUserName());
      cliente.subscribe((data:any)=>{
        const id = this.idusuario = data.idCliente;
        this.storage.set('datosCliente',data);
        this.storage.set('idCliente',id);
      });
      return cliente; //'obtenerCliente' + this.TokenService.getUserName()
      //+ this.TokenService.getUserName
    }
    public async cargarCliente(): Promise<Cliente>{
      return await this.storage.get('idCliente');
    }
    public async cargarDatosCliente(): Promise<Cliente>{
      return await this.storage.get('datosCliente');
    }
}
