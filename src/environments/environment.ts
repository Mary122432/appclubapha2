// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  //ENDPOIN DEL APARTADO NOTICIAS
  getNoticiasJoomla: 'api/v1/category/list/19/content',
  direccionJoomla:  'http://c42f-187-188-205-121.ngrok.io/clubalpha/',
  token: `VGZAjMy3NliDlDNMTZjMzDm41ZFT7RYQMhj3DRN`,
  //RAZÓN SOCIAL
  club: 'sports',
  //ENDPOINT GLOBAL
  //global: 'http://localhost:8080/',  //LOCAL
  global: 'http://app.sportsplaza.mx/' // URL SERVIDOR
  //global: 'http://192.168.20.57:8090/' //URL SERVIDOR PRUEBAS

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
